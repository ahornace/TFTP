#ifndef TEST_H
#define TEST_H

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>
#include <string.h>
#include <libgen.h> //for basename
#include <assert.h>
#include "client.h"
#include "server.h"

/**
 * Test for receiving correct error opcode. 
 * Test first creates file, starts server and tries to rewrite the same file. Server should not let him do it.
 */
void file_already_exists_test();

/**
 * Test for receiving correct error opcode.
 * Test runs server and tries to download nonexistent file. Server should return proper error msg.
 */
void file_not_found_test();

/**
 * Test for correct sending of the file.
 */
void correct_sent_test();

#endif