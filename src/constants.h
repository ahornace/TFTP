#ifndef CONSTANTS_H
#define CONSTANTS_H

#define FILENAME_MAX_LENGTH 256 //ext has maximum length 255 so plus 1 for null
#define PACKET_MAX_LENGTH 516 //512 bytes for data, 2 bytes for opcode, 2 bytes for packet num
#define MODE_MAX_LENGTH 9 //netascii = 8 + null char -> 9
#define RETRY_COUNT 3
#define POLL_WAIT 1000 //1 second

#endif