#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <err.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h> // for close
#include <sys/poll.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include "tftp.h"

/**
 * Starts tftp server.
 * @param port port to start server at
 * @param home directory to save and read files from
 * @param test_flag specifies if the server was started from the test
 */
void start_server(char *port, char *home, int test_flag);

#endif