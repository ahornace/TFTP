#ifndef CLIENT_H
#define CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <err.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h> // for close
#include <sys/poll.h>
#include "tftp.h"
#include "constants.h"

/**
 * Reads file from the server.
 * @param sock socket of the communication
 * @param filename file name on the server
 * @param res addrinfo of the server
 * @param file_to_save path under which to save the file
 * @param mode mode of the transfer
 * @return program return value
 */
int read_file(int sock, char *filename, struct addrinfo *res, char *file_to_save, mode_type mode);

/** 
 * Reads file from the server.
 * @param sock socket of the communication
 * @param new_filename file name on the server 
 * @param res addrinfo of the server
 * @param file_to_send path to the file for the sending
 * @param mode mode of the transfer
 * @return program return value
 */
int send_file(int sock, char *new_filename, struct addrinfo *res, char *file_to_send, mode_type mode);

#endif