C_FILES=$(wildcard src/*.c)
OBJ_FILES=$(addprefix obj/,$(notdir $(C_FILES:.c=.o)))
CFLAGS=-std=c99 -Wall -D_POSIX_C_SOURCE=200809L

all: init program

program: $(OBJ_FILES)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

obj/%.o : src/%.c
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean init

init:
	mkdir -p obj

clean:
	rm -rf obj/*.o
	rmdir obj
	rm -f program
