# TFTP Server and Client Implementation for Unix systems

This project is very simple implementation of TFTP protocol according to the https://tools.ietf.org/html/rfc1350

## Compilation
For compilation is used GNU make. Included makefile supports the following commands:

* program - builds final program
* clean - deletes all generated object and executable files

## Usage
The final program can be run as tftp client and tftp server. The differences how and what should the program do are specified as his arguments, which are:

* -s = runs this program as tftp server. If it's not present, then this program starts as a client
* -a = specifies address of the server to connect to and can be present only if program is started as a client
* -p <value> = port on which the server runs or client connects to. If not present, then standard 69 port is selected
* -d = tells program if it should print debug information
* -w <value> = writes given file to the server
* -r <value> = reads given file from server
* -h <value> = specifies home directory for the server
* -n = specifies mode for the transfer as netascii otherwise it is octet
* -f <value> = if write request then specifies the name of the file on the server. If read request then specifies the name of the file on the client
* -u = prints usage
* -t = runs tests and exits the program

## Notes

Mail mode is not implemented because it is considered to be obsolete. 

## Tests
Tests are run by the option -t of the program. They cover following functionality:

* trying to rewrite file on the server
* trying to download nonexistent file
* sending one file and comparing that it was sent correctly

## Known issues

## Tested on
* Mac OS X 10.11.5
* Linux (Gentoo and Ubuntu)